import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  var latitude = 'Getting Latitude..'.obs;
  var longitude = 'Getting Longitude..'.obs;
  late StreamSubscription<Position> streamSubscription;
  var serviceEnabled = false.obs;
  var listGps = <String>[].obs;

  final storage = const FlutterSecureStorage();
  @override
  void onInit() async {
    super.onInit();
    Timer.periodic(
      const Duration(seconds: 1),
      (timer) {
        getLocation();
        update();
      },
    );
    readAll();
  }

  getLocation() async {
    LocationPermission permission;
    // Test if location services are enabled.
    serviceEnabled.value = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled.value) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      await Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.flutter build apk --split-per-abi
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    streamSubscription =
        Geolocator.getPositionStream().listen((Position position) {
      latitude.value = 'Latitude : ${position.latitude}';
      longitude.value = 'Longitude : ${position.longitude}';
    });
  }

  Future<void> readAll() async {
    final all = await storage.readAll(aOptions: _getAndroidOptions());
    listGps.value =
        all.entries.map((entry) => entry.key).toList(growable: false);
    listGps.value = listGps.reversed.toList();
    update();
  }

  void deleteAll() async {
    await storage.deleteAll(aOptions: _getAndroidOptions());
    readAll();
  }

  void addNewItem(String positionName) async {
    await storage.write(
        key:
            " Position: $positionName | ${longitude.value} | ${latitude.value} ",
        value: "",
        aOptions: _getAndroidOptions());
    readAll();
  }

  void deleteItem(String key) async {
    storage.delete(key: key, aOptions: _getAndroidOptions());
    readAll();
  }

  AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );
}
