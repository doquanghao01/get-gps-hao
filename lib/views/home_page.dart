import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_gps/controller/home_controller.dart';

import 'widgets/home_button.dart';
import 'widgets/home_icon.dart';
import 'widgets/home_text.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController textController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: const Text("GPS"),
      ),
      body: GetBuilder<HomeController>(
        initState: (state) => Get.find<HomeController>(),
        builder: (homeController) => Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  const HomeText(text: "Vị trí"),
                  const SizedBox(width: 20),
                  Expanded(
                    child: TextField(
                      controller: textController,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Nhập tên vị trí',
                      ),
                      style: const TextStyle(fontSize: 20),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              HomeText(text: homeController.longitude.value, size: 25),
              const SizedBox(height: 20),
              HomeText(text: homeController.latitude.value, size: 25),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  HomeButton(
                    text: 'Save',
                    press: () {
                      textController.text.toString().isEmpty
                          ? diaNotification("Vui lòng nhập tên vị trí !")
                          : homeController
                              .addNewItem(textController.text.toString());
                      textController.clear();
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  HomeButton(
                    text: "Copy all",
                    buttomColor: Colors.blue,
                    press: () {
                      snackbarNotification("Thông báo", "Copy thành công !");
                      Clipboard.setData(
                        ClipboardData(
                          text: homeController.listGps.toString(),
                        ),
                      );
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  HomeButton(
                    text: "Delete All",
                    buttomColor: Colors.red,
                    press: () {
                      homeController.listGps.isEmpty
                          ? diaNotification("Không có vị trí để xóa")
                          : warning(context, () {
                              homeController.deleteAll();
                              Navigator.pop(context, 'OK');
                            }, 'Bạn có muốn xóa tất cả các vị trí ko ?');
                    },
                  ),
                ],
              ),
              listPosition(homeController),
            ],
          ),
        ),
      ),
    );
  }

  Expanded listPosition(HomeController homeController) {
    return Expanded(
      child: ListView.builder(
        itemCount: homeController.listGps.length,
        itemBuilder: (context, index) {
          return Row(
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(
                    top: 10,
                    bottom: 10,
                    right: 10,
                  ),
                  child: Text(
                    homeController.listGps[index],
                    style: const TextStyle(fontSize: 20),
                  ),
                ),
              ),
              HomeIcon(
                press: () {
                  snackbarNotification(
                    "Thông báo",
                    "Copy thành công!",
                  );
                  Clipboard.setData(
                    ClipboardData(
                      text: homeController.listGps[index],
                    ),
                  );
                },
              ),
              HomeIcon(
                iconData: Icons.clear,
                color: Colors.red,
                press: () {
                  warning(context, () {
                    homeController.deleteItem(homeController.listGps[index]);
                    Navigator.pop(context, 'OK');
                  }, 'Bạn có muốn xóa vị trí này ko ?');
                },
              ),
            ],
          );
        },
      ),
    );
  }

  //Show dialog notification
  Future<dynamic> diaNotification(String text) {
    return Get.defaultDialog(
      title: "Thông báo",
      titleStyle: const TextStyle(color: Colors.blue),
      middleText: text,
      actions: [
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            primary: Colors.blue,
          ),
          onPressed: () {
            Get.back();
          },
          child: const Text("Ok"),
        ),
      ],
    );
  }

  //Show dialog warning
  Future<String?> warning(
      BuildContext context, VoidCallback press, String text) {
    return showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: const Text(
          'Cảnh báo',
          style: TextStyle(color: Colors.red),
        ),
        content: Text(text),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: press,
            child: const Text(
              'OK',
              style: TextStyle(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }

  //Show notification sackbar
  SnackbarController snackbarNotification(String title, String message) {
    return Get.snackbar(
      title,
      message,
      snackPosition: SnackPosition.BOTTOM,
    );
  }
}
