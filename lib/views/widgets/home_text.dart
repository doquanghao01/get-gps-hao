import 'package:flutter/material.dart';

class HomeText extends StatelessWidget {
  const HomeText({
    Key? key,
    required this.text,
    this.size = 20,
  }) : super(key: key);
  final String text;
  final double size;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(fontSize: size, fontWeight: FontWeight.bold),
    );
  }
}
