import 'package:flutter/material.dart';

class HomeIcon extends StatelessWidget {
  const HomeIcon({
    Key? key,
    required this.press,
    this.iconData = Icons.copy,
    this.color = Colors.blue,
  }) : super(key: key);
  final VoidCallback press;
  final IconData iconData;
  final Color color;
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 40,
      width: 40,
      child: IconButton(
        onPressed: press,
        icon: Icon(
          iconData,
          color: color,
        ),
      ),
    );
  }
}
